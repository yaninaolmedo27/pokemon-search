import axios from 'axios'

//Constants
const initialData = {
    array : []
}

//Types
const GET_POKEMONS_SUCCESS = 'GET_POKEMONS_SUCCESS';

//Reducer
export default function pokeReducer(state = initialData , action) {
    switch (action.type) {
        case GET_POKEMONS_SUCCESS:
            return {
                ...state,
                array: action.payload
            }
    
        default:
            return state
    }
}

//Actions
export const obtenerPokemonsAction = () => async (dispatch, getState) => {
    try {
        const res = await axios.get('https://pokeapi.co/api/v2/pokemon?offset=0&limit=20')
        dispatch({
            type: GET_POKEMONS_SUCCESS,
            payload: res.data.results
        })
    } catch (error) {
        console.log(error)
    }
}